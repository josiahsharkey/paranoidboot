{ stdenv, bison, flex
# options
, macAddress ? "00:de:ad:c0:ff:ee"
, model ? "82579LM"
}:

stdenv.mkDerivation {
  name = "gbe.bin";

  src = (builtins.fetchGit {
    url = "https://review.coreboot.org/coreboot.git";
    rev = "219caf83580a86acf073f73662356a078bd96244";
  }).outPath + "/util/bincfg"; 

  buildInputs = [ bison flex ];
  makeFlags = [ "bincfg"  "gen-gbe-${model}" ];

  configurePhase = stdenv.lib.concatImapStrings
    (i: n: ''
      sed -i gbe-${model}.set -e \
      's@\("mac_address_${toString (i - 1)}" = 0x\)[0-9A-F]\+@\1${n}@'
    '')
    (stdenv.lib.splitString ":" macAddress);

  installPhase = "install -Dm644 flashregion_3_gbe.bin $out";
  
  meta = with stdenv.lib; {
    description = "The Intel Gigabit Ethernet configuration";
    homepage = "https://www.coreboot.org";
    license = licenses.gpl2Only;
    platforms = platforms.all;
  };
}
