# Nixboot

### Simple declarative and reproducible coreboot images

This repository contains a minimal interface to build coreboot images using
Nix. Coreboot is already near 100% binary reproducible and can be configured
via [Kconfig](https://doc.coreboot.org/getting_started/kconfig.html); however
the process is somewhat involved: it requires maintaining a local checkout of
the coreboot source, several config files and shell scripts. Nixboot automates
this process and allows to control it declaratively from a single file.

## Supported features

- Changing target architecture
- Building an Intel firmware
- Running `me_cleaner`
- Generating blobs using `bincfg`
- SeaBios as payload

Other payloads (including secondaries) can be very easily added.


## Build instructions

### 1. Fetching sources

Go to the board [status page](https://coreboot.org/status/board-status.html)
and find the latest tested revision for your mainboard, or pick one yourself.
Next, to fetch all the source archives needed to build coreboot, run:

    scripts/gen-sources.sh REV > sources.nix

where REV is the revision you chose.

This may take a while depending on your connection: consider coreboot will
build its own compiler toolchain from source. 

### 2. Configuring

Once the `sources.nix` has been generated, to configure coreboot and the
payload, write a `board.nix`, like this one:
```
{ pkgs ? import <nixpkgs> { } }:

pkgs.callPackage ./coreboot.nix {
  # known good revision
  rev = "d8bc5c127ad13ed0475bbf7465f6ba56a5fa34ee";
  sources = pkgs.callPackage ./sources.nix { };

  conf = {
    # mainboard
    vendor.lenovo = true;
    board.lenovo-x230 = true;

    # drivers
    pciexp = {
      hotplug      = true;
      clk-pm       = true;
      l1-sub-state = true;
    };

    # display
    generic-linear-framebuffer = true;
    linear-framebuffer = {
      max-width  = 1024;
      max-height = 768;
    };

    # payload
    seabios = {
      revision    = true;
      revision-id = "ef88eeaf052c8a7d28c5f85e790c5e45bcffa45e";
    };
  };

}
```
The configuration options can be found on the coreboot
[website](https://coreboot.org/status/kconfig-options.html). The names are
case-insensitive and options with the same prefix (for example `PCIEXP_`) can
be conveniently grouped into a set.

Valid option values includes:

  - booleans
  - strings
  - integers
  - paths (will be copied into the Nix store)
  - derivations (will be converted to store paths)





### 3. Building

Simply run

    nix build -f board.nix coreboot

This will build the toolchain, the full configuration and use them to build
coreboot itself. The output consist of:

    result/
    ├── config
    ├── coreboot.rom
    └── defconfig

where `config` contains the full coreboot configuration, `defconfig` only the
non-default ones and `coreboot.rom` is the final image, ready to be flashed.

It's also possible to only build the toolchain using the `toolchain` attribute
and see the defconfig before building with `defConfig`.


## License

Copyright (C) 2021 Michele Guerini Rocco

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see http://www.gnu.org/licenses/.
