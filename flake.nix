{
  description = "builds as many open source alternatives to firmware as possible and replaces as much of it as possible with Linux";
  inputs = import ./inputs;
  outputs = import ./outputs;
}