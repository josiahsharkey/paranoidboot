{ pkgs ? import <nixpkgs> { } }:

pkgs.callPackage ../../coreboot.nix {
  # last known good
  rev = "4bd69273882c66667590f098f2b9bb916b6c1a44";
  sources = pkgs.callPackage ../charlie/sources.nix { };

  conf = {
    # payload
    seabios = {
      revision    = true;
      revision-id = "ef88eeaf052c8a7d28c5f85e790c5e45bcffa45e";
      debug-level = 3;
    };
  };

}
