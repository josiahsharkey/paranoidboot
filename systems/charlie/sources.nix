{ fetchurl }:
{ rev = "4bd69273882c66667590f098f2b9bb916b6c1a44";
  sha256 = "162m24zlh9lwi4v9bi9gq6s37gkb96l0iyznd1l8y93iyzfcaqg9";
  toolchain = [
    (fetchurl {
      url = "https://ftpmirror.gnu.org/gmp/gmp-6.1.2.tar.xz";
      sha256 = "04hrwahdxyqdik559604r7wrj9ffklwvipgfxgj4ys4skbl6bdc7";
    })
    (fetchurl {
      url = "https://ftpmirror.gnu.org/mpfr/mpfr-4.0.2.tar.xz";
      sha256 = "12m3amcavhpqygc499s3fzqlb8f2j2rr7fkqsm10xbjfc04fffqx";
    })
    (fetchurl {
      url = "https://ftpmirror.gnu.org/mpc/mpc-1.1.0.tar.gz";
      sha256 = "0biwnhjm3rx3hc0rfpvyniky4lpzsvdcwhmcn7f0h4iw2hwcb1b9";
    })
    (fetchurl {
      url = "https://ftpmirror.gnu.org/gcc/gcc-8.3.0/gcc-8.3.0.tar.xz";
      sha256 = "0b3xv411xhlnjmin2979nxcbnidgvzqdf4nbhix99x60dkzavfk4";
    })
    (fetchurl {
      url = "https://ftpmirror.gnu.org/binutils/binutils-2.33.1.tar.xz";
      sha256 = "1grcf8jaw3i0bk6f9xfzxw3qfgmn6fgkr108isdkbh1y3hnzqrmb";
    })
    (fetchurl {
      url = "https://ftpmirror.gnu.org/gdb/gdb-8.3.1.tar.xz";
      sha256 = "1i2pjwaafrlz7wqm40b4znr77ai32rjsxkpl2az38yyarpbv8m8y";
    })
    (fetchurl {
      url = "https://acpica.org/sites/acpica/files/acpica-unix2-20200110.tar.gz";
      sha256 = "1hb4g6r7w8s4bhlkk36fmb4qxghnrwvad7f18cpn6zz0b4sjs7za";
    })
    (fetchurl {
      url = "https://www.python.org/ftp/python/3.8.1/Python-3.8.1.tar.xz";
      sha256 = "1s4lwn5vzsajlc88m6hkghsvnjw4d00l2dsgng0m2w6vyqbl32bm";
    })
    (fetchurl {
      url = "https://downloads.sourceforge.net/sourceforge/expat/expat-2.2.9.tar.bz2";
      sha256 = "0dx2m58gkj7cadk51lmp54ma7cqjhff4kjmwv8ks80j3vj2301pi";
    })
    (fetchurl {
      url = "https://releases.llvm.org/9.0.0/llvm-9.0.0.src.tar.xz";
      sha256 = "117ymdz1by2nkfq1c2p9m4050dp848kbjbiv6nsfj8hzy9f5d86n";
    })
    (fetchurl {
      url = "https://releases.llvm.org/9.0.0/cfe-9.0.0.src.tar.xz";
      sha256 = "0426ma80i41qsgzm1qdz81mjskck426diygxi2k5vji2gkpixa3v";
    })
    (fetchurl {
      url = "https://releases.llvm.org/9.0.0/compiler-rt-9.0.0.src.tar.xz";
      sha256 = "03ni43lbkp63lr3p6sc94dphqmvnz5av5mml0xmk930xvnbcvr2n";
    })
    (fetchurl {
      url = "https://releases.llvm.org/9.0.0/clang-tools-extra-9.0.0.src.tar.xz";
      sha256 = "045cldmcfd8s33wyjlviifgpnw52yqicd6v4ysvdg4i96p78c77a";
    })
    (fetchurl {
      url = "https://ftpmirror.gnu.org/make/make-4.2.1.tar.bz2";
      sha256 = "12f5zzyq2w56g95nni65hc0g5p7154033y2f3qmjvd016szn5qnn";
    })
    (fetchurl {
      url = "https://cmake.org/files/v3.16/cmake-3.16.2.tar.gz";
      sha256 = "1ag65ignli58kpmji6gjhj8xw4w1qdr910i99hsvx8hcqrp7h2cc";
    })
    (fetchurl {
      url = "https://www.nasm.us/pub/nasm/releasebuilds/2.14.02/nasm-2.14.02.tar.bz2";
      sha256 = "1g409sr1kj7v1089s9kv0i4azvddkcwcypnbakfryyi71b3jdz9l";
    })
  ];
}