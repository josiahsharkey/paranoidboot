{ pkgs ? import <nixpkgs> { } }:

pkgs.callPackage ../../coreboot.nix {
  # last known good
  rev = "4bd69273882c66667590f098f2b9bb916b6c1a44";
  sources = pkgs.callPackage ./sources.nix { };

  conf = {
    # main
    vendor.lenovo = true;
    board.lenovo-x230 = true;

    # drivers
    tpm-deactivate = true;
    drivers-ps2-keyboard = true;
    pciexp = {
      hotplug      = true;
      clk-pm       = true;
      l1-sub-state = true;
    };

    # display
    generic-linear-framebuffer = true;
    linear-framebuffer = {
      max-width  = 1366;
      max-height = 768;
    };

    # payload
    seabios = {
      revision    = true;
      revision-id = "ef88eeaf052c8a7d28c5f85e790c5e45bcffa45e";
    };

    # blobs
    have-ifd-bin = true;
    have-me-bin  = true;
    have-gbe-bin = true;
    ifd-bin-path = ./blobs/ifd.bin;
     me-bin-path = ./blobs/me.bin;
    gbe-bin-path = pkgs.callPackage ../../gbe.nix { macAddress = "e0:36:eb:9b:6c:3e"; };

    # neutralise me
    check-me       = true;
    use-me-cleaner = true;
  };

  postCommands = ''
    # add SeaBios BMP bootsplash
    ./build/cbfstool build/coreboot.rom add -f ${./bootsplash.bmp} -c lzma -n bootsplash.bmp.lzma -t raw
  '';

}
